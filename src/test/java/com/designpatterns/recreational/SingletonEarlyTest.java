package com.designpatterns.recreational;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SingletonEarlyTest {

    @Test
    public void getInstance() {
        assertEquals(SingletonEarly.getInstance(), SingletonEarly.getInstance());
    }
}