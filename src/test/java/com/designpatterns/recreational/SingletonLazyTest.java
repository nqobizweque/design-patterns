package com.designpatterns.recreational;

import org.junit.Test;

import static org.junit.Assert.*;

public class SingletonLazyTest {

    @Test
    public void getInstance() {
        assertEquals(SingletonLazy.getInstance(), SingletonLazy.getInstance());
    }
}