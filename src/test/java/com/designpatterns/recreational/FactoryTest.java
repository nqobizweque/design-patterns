package com.designpatterns.recreational;

import jdk.nashorn.internal.objects.annotations.Property;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FactoryTest {

    @Property
    private Factory factory;

    @Before
    public void setup() {
        factory = new Factory();
    }

    @Test
    public void testCreateProduct() {
        for (int i = 0; i < 3; i++) {
            assertTrue(Product.class.isAssignableFrom(factory.createProduct().getClass()));
        }
    }
}