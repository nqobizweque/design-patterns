package com.designpatterns.recreational;

import java.util.Random;

public class Factory {
    public Product createProduct() {
        int number = new Random().nextInt(10);
        if (number % 2 == 0) {
            return new ProductA();
        }
        return new ProductB();
    }
}
