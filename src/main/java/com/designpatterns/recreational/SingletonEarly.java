package com.designpatterns.recreational;

public class SingletonEarly {
    private static SingletonEarly instance = new SingletonEarly();
    private SingletonEarly() {}
    public static SingletonEarly getInstance() {
        return instance;
    }
}
